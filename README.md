# MethTimer

[![PyPI version](https://badge.fury.io/py/methtimer.svg)](https://badge.fury.io/py/methtimer)

>A  python decorator that calculates the time of execution for any method.

## Install
```
$ pip install methtimer
```

## Usage 
<img src="https://gitlab.com/AbiramK/methtimer/raw/master/assets/usage.png" width="300">

## License

[GNU][License]

[LICENSE]: https://www.gnu.org/licenses/gpl-3.0.en.html